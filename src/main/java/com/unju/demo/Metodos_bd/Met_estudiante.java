/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.Metodos_bd;

import com.unju.demo.Entity.estudiantes;
import com.unju.demo.interface_Acciones.Iestudiante;
import com.unju.demo.interface_Est_Pres_Lib.IestudianteRepo;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Miguel Torrez
 */
@Service
public class Met_estudiante implements Iestudiante{
    @Autowired
    private IestudianteRepo data;

    @Override
    public List<estudiantes> listar_est() {
        return (List<estudiantes>) data.findAll();
    }

    @Override
    public Optional<estudiantes> listarId_est(int Nro_ID) {
       return data.findById(Nro_ID);
    }

    @Override
    public int save_est(estudiantes e) {
        int res = 0;
        estudiantes estudiante = data.save(e);
        if (!estudiante.equals(null)) {
            res = 1;
        }
        return res;
    }

    @Override
    public void delete_est(int Nro_ID) {
        data.deleteById(Nro_ID);
    }
}
