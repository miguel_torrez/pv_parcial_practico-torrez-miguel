package com.unju.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpTorrezApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpTorrezApplication.class, args);
	}

}
