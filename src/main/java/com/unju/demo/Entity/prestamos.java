/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Miguel Torrez
 */
@Entity
@Table(name="Prestamos")
public class prestamos {
    
    @Id 
    @Column(name="ID_PRESTAMO")
    private int Id_prestamo;
    
    @ManyToOne
    @JoinColumn(name = "Nro_ID")
    private estudiantes Nro_ID;
    
    @ManyToOne
    @JoinColumn(name = "ISBN")
    private libros isbn;
    
    @Column(name="FECHA_PRESTAMO")
    private String fecha_prestamo;

    public prestamos() {
    }

    public prestamos(int Id_prestamo, estudiantes Nro_ID, libros isbn, String fecha_prestamo) {
        this.Id_prestamo = Id_prestamo;
        this.Nro_ID = Nro_ID;
        this.isbn = isbn;
        this.fecha_prestamo = fecha_prestamo;
    }

    public int getId_prestamo() {
        return Id_prestamo;
    }

    public void setId_prestamo(int Id_prestamo) {
        this.Id_prestamo = Id_prestamo;
    }

    public estudiantes getNro_ID() {
        return Nro_ID;
    }

    public void setNro_ID(estudiantes Nro_ID) {
        this.Nro_ID = Nro_ID;
    }

    public libros getIsbn() {
        return isbn;
    }

    public void setIsbn(libros isbn) {
        this.isbn = isbn;
    }

    public String getFecha_prestamo() {
        return fecha_prestamo;
    }

    public void setFecha_prestamo(String fecha_prestamo) {
        this.fecha_prestamo = fecha_prestamo;
    }
}
