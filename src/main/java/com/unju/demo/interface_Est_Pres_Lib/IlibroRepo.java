/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.interface_Est_Pres_Lib;

import com.unju.demo.Entity.libros;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Miguel Torrez
 */
@Repository
public interface IlibroRepo extends CrudRepository<libros, Integer>{
    
}
