/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.Metodos_bd;

import com.unju.demo.Entity.prestamos;
import com.unju.demo.interface_Acciones.Iprestamo;
import com.unju.demo.interface_Est_Pres_Lib.IprestamoRepo;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Miguel Torrez
 */
@Service
public class Met_prestamo implements Iprestamo{
    
    @Autowired
    private IprestamoRepo data;

    @Override
    public List<prestamos> listar_pre() {
        return (List<prestamos>) data.findAll();
    }

    @Override
    public Optional<prestamos> listarId_pre(int Id_prestamo) {
       return data.findById(Id_prestamo);
    }

    @Override
    public int save_pre(prestamos p) {
        int res = 0;
        prestamos prestamo = data.save(p);
        if (!prestamo.equals(null)) {
            res = 1;
        }
        return res;
    }

    @Override
    public void delete_pre(int Id_prestamo) {
        data.deleteById(Id_prestamo);
    }
}
