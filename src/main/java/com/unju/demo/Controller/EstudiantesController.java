/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.Controller;

import com.unju.demo.Entity.estudiantes;
import com.unju.demo.interface_Acciones.Iestudiante;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Miguel Torrez
 */
@Controller
@RequestMapping
public class EstudiantesController {
    
    @Autowired
    private Iestudiante alumno;
    
    @GetMapping("/listar_est")
    public String listar_est(Model model) {
        List<estudiantes> estudiantes = alumno.listar_est();
        model.addAttribute("estudiantes", estudiantes);
        return "listar_est";
    }
    
    @GetMapping("/listar_est/{Nro_ID}")
    public String listarId_est(@PathVariable int Nro_ID,Model model) {
        model.addAttribute("estudiante", alumno.listarId_est(Nro_ID));
        return "formulario_est";
    }
    
    @GetMapping("/nuevo_est")
    public String agregar_est(Model model) {
        model.addAttribute("estudiante", new estudiantes());
        return "formulario_est";
    }
    
    @PostMapping("/guardar_est")
    public String save_est(@Valid estudiantes e,Model model) {
        alumno.save_est(e);
        return "redirect:/listar_est";
    }
    
    @GetMapping("/eliminar_est/{Nro_ID}")
	public String delete_est(@PathVariable int Nro_ID,Model model) {
            alumno.delete_est(Nro_ID);
	return "redirect:/listar_est";
    }
        
    @GetMapping("/editar_est/{Nro_ID}")
    public String editar_est(@PathVariable int Nro_ID, Model model){
        Optional<estudiantes>estudiante=alumno.listarId_est(Nro_ID);
        model.addAttribute("estudiante", estudiante);
        return "formulario_est";
    }
    
    @GetMapping("/")
    public String page(Model model) {
        model.addAttribute("attribute", "value");
        return "index";
    }
}
