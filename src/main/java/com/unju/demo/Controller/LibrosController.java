/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.Controller;

import com.unju.demo.Entity.libros;
import com.unju.demo.interface_Acciones.Ilibro;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Miguel Torrez
 */
@Controller
@RequestMapping
public class LibrosController {
    
    @Autowired
    private Ilibro lib;
    
    @GetMapping("/listar_lib")
    public String listar_lib(Model model) {
        List<libros> libros = lib.listar_lib();
        model.addAttribute("libros", libros);
        return "listar_lib";
    }
    
    @GetMapping("/listar_lib/{isbn}")
    public String listarId_lib(@PathVariable int isbn,Model model) {
        model.addAttribute("libro", lib.listarId_lib(isbn));
        return "formulario_lib";
    }
    
    @GetMapping("/nuevo_lib")
    public String agregar_lib(Model model) {
        model.addAttribute("libro", new libros());
        return "formulario_lib";
    }
    
    @PostMapping("/guardar_lib")
    public String save_lib(@Valid libros li,Model model) {
        lib.save_lib(li);
        return "redirect:/listar_lib";
    }
    
    @GetMapping("/eliminar_lib/{isbn}")
	public String delete_lib(@PathVariable int isbn,Model model) {
            lib.delete_lib(isbn);
	return "redirect:/listar_lib";
    }
        
    @GetMapping("/editar_lib/{isbn}")
    public String editar_lib(@PathVariable int isbn, Model model){
        Optional<libros>libro=lib.listarId_lib(isbn);
        model.addAttribute("libro", libro);
        return "formulario_lib";
    }
}
