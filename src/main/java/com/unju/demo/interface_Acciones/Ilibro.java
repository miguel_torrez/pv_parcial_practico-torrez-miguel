/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.interface_Acciones;

import com.unju.demo.Entity.libros;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Miguel Torrez
 */
public interface Ilibro {
    public List<libros>listar_lib();
    public Optional<libros>listarId_lib(int isbn);
    public int save_lib(libros li);
    public void delete_lib(int isbn);
}
