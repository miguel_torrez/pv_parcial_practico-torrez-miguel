/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.interface_Acciones;

import com.unju.demo.Entity.estudiantes;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Miguel Torrez
 */
public interface Iestudiante {
    public List<estudiantes>listar_est();
    public Optional<estudiantes>listarId_est(int Nro_ID);
    public int save_est(estudiantes e);
    public void delete_est(int Nro_ID);
}
