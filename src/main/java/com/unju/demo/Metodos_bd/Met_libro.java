/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.Metodos_bd;

import com.unju.demo.Entity.libros;
import com.unju.demo.interface_Acciones.Ilibro;
import com.unju.demo.interface_Est_Pres_Lib.IlibroRepo;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Miguel Torrez
 */
@Service
public class Met_libro implements Ilibro{
    @Autowired
    private IlibroRepo data;

    @Override
    public List<libros> listar_lib() {
        return (List<libros>) data.findAll();
    }

    @Override
    public Optional<libros> listarId_lib(int isbn) {
       return data.findById(isbn);
    }

    @Override
    public int save_lib(libros li) {
        int res = 0;
        libros libro = data.save(li);
        if (!libro.equals(null)) {
            res = 1;
        }
        return res;
    }

    @Override
    public void delete_lib(int isbn) {
        data.deleteById(isbn);
    }
}
