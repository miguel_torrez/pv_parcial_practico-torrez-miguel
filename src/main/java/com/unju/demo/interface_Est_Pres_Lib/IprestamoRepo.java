/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.interface_Est_Pres_Lib;

import com.unju.demo.Entity.prestamos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Miguel Torrez
 */
@Repository
public interface IprestamoRepo extends CrudRepository<prestamos, Integer>{
    
}
