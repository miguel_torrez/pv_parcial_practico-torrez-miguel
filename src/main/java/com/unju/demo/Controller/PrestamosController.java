/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.Controller;

import com.unju.demo.Entity.estudiantes;
import com.unju.demo.Entity.libros;
import com.unju.demo.Entity.prestamos;
import com.unju.demo.interface_Acciones.Iestudiante;
import com.unju.demo.interface_Acciones.Ilibro;
import com.unju.demo.interface_Acciones.Iprestamo;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Miguel Torrez
 */
@Controller
@RequestMapping
public class PrestamosController {
    
    @Autowired
    private Iprestamo prest;
    
    @Autowired
    private Iestudiante est;
    
    @Autowired
    private Ilibro lib;
    
    @GetMapping("/listar_pre")
    public String listar_pre(Model model) {
        List<prestamos> prestamos = prest.listar_pre();
        model.addAttribute("prestamos", prestamos);
        return "listar_pre";
    }
    
    @GetMapping("/listar_pre/{Id_prestamo}")
    public String listarId_pre(@PathVariable int Id_prestamo,Model model) {
        List<estudiantes> estudiantes = est.listar_est();
        model.addAttribute("estudiantes", estudiantes);
        List<libros> libros = lib.listar_lib();
        model.addAttribute("libros", libros);
        model.addAttribute("prestamo", prest.listarId_pre(Id_prestamo));
        return "formulario_pre";
    }
    
    @GetMapping("/nuevo_pre")
    public String agregar_pre(Model model) {
        List<estudiantes> estudiantes = est.listar_est() ;
        model.addAttribute("estudiantes", estudiantes);
        
        List<libros> libros = lib.listar_lib();
        model.addAttribute("libros", libros);
        
        model.addAttribute("`prestamo", new prestamos());
        return "formulario_pre";
    }
    
    @PostMapping("/guardar_pre")
    public String save_pre(@Valid prestamos p,Model model) {
        prest.save_pre(p);
        return "redirect:/listar_pre";
    }
    
    @GetMapping("/eliminar_pre/{Id_prestamo}")
	public String delete_pre(@PathVariable int Id_prestamo,Model model) {
            prest.delete_pre(Id_prestamo);
	return "redirect:/listar_pre";
    }
        
    @GetMapping("/editar_pre/{Id_prestamo}")
    public String editar_pre(@PathVariable int Id_prestamo, Model model){
        List<estudiantes> estudiantes = est.listar_est() ;
        model.addAttribute("estudiantes", estudiantes);
        System.out.println(estudiantes.toString());
        
        List<libros> libros = lib.listar_lib();
        model.addAttribute("libros", libros);
        System.out.println(libros.toString());
        
        Optional<prestamos>prestamo=prest.listarId_pre(Id_prestamo);
        model.addAttribute("prestamo", prestamo);
        return "formulario_pre";
    }
}
