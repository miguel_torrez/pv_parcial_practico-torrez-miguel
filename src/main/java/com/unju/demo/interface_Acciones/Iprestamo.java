/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.interface_Acciones;

import com.unju.demo.Entity.prestamos;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Miguel Torrez
 */
public interface Iprestamo {
    public List<prestamos>listar_pre();
    public Optional<prestamos>listarId_pre(int Id_prestamo);
    public int save_pre(prestamos p);
    public void delete_pre(int Id_prestamo);
}
