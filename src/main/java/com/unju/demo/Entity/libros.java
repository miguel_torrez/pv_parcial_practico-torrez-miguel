/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Miguel Torrez
 */
@Entity
@Table(name="Libros")
public class libros {
    
    @Id 
    @Column(name="ISBN")
    private int isbn;
    
    @Column(name="TITULO")
    private String titulo;
    
    @Column(name="EDITORIAL")
    private String editorial;
    
    @Column(name="AUTOR")
    private String autor;

    public libros() {
    }

    public libros(int isbn, String titulo, String editorial, String autor) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.editorial = editorial;
        this.autor = autor;
    }

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
}
