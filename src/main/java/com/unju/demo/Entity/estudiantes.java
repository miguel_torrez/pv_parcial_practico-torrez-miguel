/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Miguel Torrez
 */
@Entity
@Table(name="Estudiantes")
public class estudiantes {
    @Id 
    @Column(name="Nro_ID")
    private int Nro_ID;
    
    @Column(name="NOMBRES")
    private String nombres;
    
    @Column(name="APELLIDO")
    private String apellido;
    
    @Column(name="DIRRECCION")
    private String direccion;

    public estudiantes() {
    }

    public estudiantes(int Nro_ID, String nombres, String apellido, String direccion) {
        this.Nro_ID = Nro_ID;
        this.nombres = nombres;
        this.apellido = apellido;
        this.direccion = direccion;
    }

    public int getNro_ID() {
        return Nro_ID;
    }

    public void setNro_ID(int Nro_ID) {
        this.Nro_ID = Nro_ID;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
}
